///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2023 Ericsson Telecom AB
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               STUN_Types.ttcn
//  Description:        STUN Protocol module
//  Rev:                <RnXnn>
//  Prodnr:             CNL 113 644
//  Updated:            2009-09-16
//  Contact:            http://ttcn.ericsson.se
//  Reference:          RFC 3489
///////////////////////////////////////////////
module STUN_Types {

external function f_STUN_Enc(in PDU_STUN pl_pdu, in octetstring pl_password := ''O) return octetstring;
external function f_STUN_Dec(in octetstring pl_oct) return PDU_STUN;
external function f_STUN_genTrasId() return octetstring;
external function f_STUN_genTrasId_withMagicCookie() return octetstring;
external function f_STUN_IP2oct(in charstring pl_ip) return octetstring;
external function f_STUN_oct2IP(in octetstring pl_ip) return charstring;
external function f_STUN_HMAC(in octetstring pl_string, in octetstring pl_key) return octetstring;
external function f_STUN_checkMessageIntegrity(in octetstring pl_msg, in octetstring pl_key) return boolean;
external function f_STUN_getMessageLength(in octetstring stream) return integer;

// STUN: RFC3489
type octetstring OCT4 length(4) with {
variant "FIELDLENGTH(4)"
}

type octetstring OCT1 length(1) with {
variant "FIELDLENGTH(1)"
}

type octetstring OCT2 length(2) with {
variant "FIELDLENGTH(2)"
}

type octetstring OCT16 length(16) with {
variant "FIELDLENGTH(16)"
}

type octetstring OCT20 length(20) with {
variant "FIELDLENGTH(20)"
}

type integer UINT16 (0..65535) with {
variant "FIELDLENGTH(16)"
variant "BYTEORDER(last)"
}

type OCT20 Attr_Oct160

type OCT4 Attr_Oct32

type OCT1 Attr_Oct8

type OCT2 Attr_Oct16

type enumerated STUN_Family 
{
  IP4 (1)
} with {
  variant "FIELDLENGTH(8)"
  variant "BYTEORDER(last)"
  variant "COMP(2scompl)"
}
//
// 11.2 STUN Message Attributes
//

// RFC 3489 11.2.1
// Attr: MAPPED-ADDRESS (1)
// RFC 3489 11.2.2
// Attr: RESPONSE-ADDRESS (2)
// RFC 3489 11.2.3
// Attr: CHANGED-ADDRESS (3)
// RFC 3489 11.2.5
// Attr: SOURCE-ADDRESS (5)
// RFC 3489 11.2.11
// Attr: REFLECTED-FROM (11)
type record Stun_Address 
{
  STUN_Family   family,
  UINT16	port_id,
  Attr_Oct32	ip_address
} with { variant (family) "FIELDLENGTH(16)"
  	 variant (family) "ALIGN(right)"
}


// RFC 3489 11.2.4
// Attr: CHANGE-REQUEST (4)
type enumerated Stun_Change_Request 
{
  NO_CHANGE   (0),
  CHANGE_PORT (2),
  CHANGE_IP   (4),
  CHANGE_BOTH (6)
} with {
  variant "FIELDLENGTH(32)"
  variant "BYTEORDER(last)"
  variant "COMP(2scompl)"
}

// RFC 3489 11.2.6
// Attr: USERNAME (6)
type charstring Stun_Username 
with { variant "PADDING(dword32)"}

// RFC 3489 11.2.7
// Attr: PASSWORD (7)
type octetstring Stun_Password 
with { variant "PADDING(dword32)"}

// RFC 3489 11.2.8
// Attr: MESSAGE-INTEGRITY (8)
type Attr_Oct160 Stun_Message_Integrity

// RFC 3489bis-13 14.7
// Attr: REALM (20)
type charstring Stun_Realm
with { variant "PADDING(dword32)"}

// RFC 3489bis-13 14.8
// Attr: NONCE (21)
type octetstring Stun_Nonce
with { variant "PADDING(dword32)"}

type enumerated Stun_Error_Code_Type
{
  BAD_REQUEST 		  (1024),
  UNAUTHORIZED		  (1025),
  UNKNOWN_ATTRIBUTE	  (1044),
  STALE_CREDENTIALS	  (1054),
  INTEGRITY_CHECK_FAILURE (1055),
  MISSING_USERNAME	  (1056),
  USE_TLS		  (1057),
  SERVER_ERROR		  (1280),
  GLOBAL_FAILURE	  (1536)
} with {
  variant "FIELDLENGTH(32)"
  variant "BYTEORDER(last)"
  variant "COMP(2scompl)"
}

// RFC 3489 11.2.9
// Attr: ERROR-CODE (9)
type record Stun_Error_Code {
  Stun_Error_Code_Type	error_code_tpye,
  charstring		reason_phrase
}
with { variant (reason_phrase) "PADDING(dword32)"}

type Attr_Oct16 STUN_Unknown_Attr_Data;

// RFC 3489 11.2.10
// Attr: UNKNOWN-ATTRIBUTES (10)
type set of STUN_Unknown_Attr_Data Stun_Unknown_Attributes
with { variant "PADDING(dword32)"}

type union STUN_Attr_data
{
  Stun_Address 			attr_Mapped_Address,
  Stun_Address			attr_Response_Address,
  Stun_Address			attr_Changed_Address,
  Stun_Change_Request		attr_Change_Request,
  Stun_Address			attr_Source_Address,
  Stun_Username			attr_Username,
  Stun_Password			attr_Password,
  Stun_Message_Integrity	attr_Message_Integrity,
  Stun_Error_Code		attr_Error_Code,
  Stun_Unknown_Attributes	attr_Unknown_Attributes,
  Stun_Address			attr_Reflected_From,
  Stun_Address                  attr_Xor_Mapped_Address,
  Stun_Realm                    attr_Realm,
  Stun_Nonce                    attr_Nonce
}

type enumerated STUN_Attr_type {
  MAPPED_ADDRESS      (1),
  RESPONSE_ADDRESS    (2),
  CHANGE_REQUEST      (3),
  SOURCE_ADDRESS      (4),
  CHANGED_ADDRESS     (5),
  USERNAME            (6),
  PASSWORD            (7),
  MESSAGE_INTEGRITY   (8),
  ERROR_CODE          (9),
  UNKNOWN_ATTRIBUTES (10),
  REFLECTED_FROM     (11),
  REALM              (20), //RFC 3489bis-13
  NONCE              (21), //RFC 3489bis-13
  XOR_MAPPED_ADDRESS (32)  //RFC 3489bis-13
} with {
  variant "FIELDLENGTH(16)"
  variant "BYTEORDER(last)"
  variant "COMP(2scompl)"
}

type record STUN_Attr {
  STUN_Attr_type	attr_type,
  UINT16		attr_length,
  STUN_Attr_data	attr_data 
} with {
  variant "PADDING(dword32)"
  variant (attr_length) "LENGTHTO(attr_data)"
  variant (attr_data) "CROSSTAG(
    attr_Mapped_Address, 	attr_type = MAPPED_ADDRESS;
    attr_Response_Address,	attr_type = RESPONSE_ADDRESS;
    attr_Changed_Address, 	attr_type = CHANGED_ADDRESS;
    attr_Change_Request,	attr_type = CHANGE_REQUEST;
    attr_Source_Address,	attr_type = SOURCE_ADDRESS;
    attr_Username,		attr_type = USERNAME;
    attr_Password,		attr_type = PASSWORD;
    attr_Message_Integrity,	attr_type = MESSAGE_INTEGRITY;
    attr_Error_Code,		attr_type = ERROR_CODE;
    attr_Unknown_Attributes,	attr_type = UNKNOWN_ATTRIBUTES;
    attr_Reflected_From,        attr_type = REFLECTED_FROM
  )"
}

type set of STUN_Attr Attr_List

type enumerated STUN_Msg_Type {
 Binding_Request                (1),
 Binding_Response             (257),
 Binding_Error_Response       (273),
 Shared_Secret_Request          (2),
 Shared_Secret_Response       (258),
 Shared_Secret_Error_Response (274)
} with {
  variant "FIELDLENGTH(16)"
  variant "BYTEORDER(last)"
  variant "COMP(2scompl)"
}

type record STUN_Header {
  STUN_Msg_Type   msg_type,
  UINT16	  msg_length,
  OCT16		  trans_id
}

type record PDU_STUN {
 	 STUN_Header 	header,
 	 Attr_List	attr_list
} with {
  variant "PADDING(dword32)"
  variant (header) "LENGTHTO(attr_list)"
  variant (header) "LENGTHINDEX(msg_length)"
}
 
} with { encode "RAW" } // End module
