#!/bin/sh

sed -e '
s/OPENSSL_DIR = $(TTCN3_DIR)/#OPENSSL_DIR = $(TTCN3_DIR)/g
s/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include -I$(OPENSSL_DIR)\/include/g
' <$1 >$2
